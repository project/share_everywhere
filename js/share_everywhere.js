(function ($, Drupal, once) {
  'use strict';

  Drupal.behaviors.shareEverywhere = {
    attach: function (context, settings) {
      $(once('se-activation', '.se-trigger img', context)).click(function (e) {
        const links = $(this).parent().parent().find('.se-links');
        $(links).toggleClass('se-active');
        $(links).toggleClass('se-inactive');
      });
      $(once('se-link-copy', '.se-link.copy', context)).click(function () {
        const url = window.location.href;

        if (window.clipboardData && window.clipboardData.setData) {
          // IE specific to prevent textarea being shown while dialog is visible.
          return clipboardData.setData("Text", url);
        }
        else if (document.queryCommandSupported && document.queryCommandSupported('copy')) {
          const textarea = document.createElement('textarea');
          textarea.textContent = url;
          textarea.style.position = 'fixed';

          document.body.appendChild(textarea);

          textarea.focus();
          textarea.select();
          try {
            return document.execCommand('copy');
          }
          catch (ex) {
            return false;
          }
          finally {
            document.body.removeChild(textarea);
          }
        }
      });
    }
  };
})(jQuery, Drupal, once);
